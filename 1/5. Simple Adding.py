def SimpleAdding(num):
    num = int(num)
    res = 0
    while num != 0:
        res += num
        num -= 1
    return res


print(SimpleAdding(input()))
