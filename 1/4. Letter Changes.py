def LetterChanges(str):
    res = ""
    for i in str:
        if i.isalpha():
            res += chr(ord(i) + 1)
        else:
            res += i
    return res


print(LetterChanges(input()))
